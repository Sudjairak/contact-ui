/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jainch
 */
public class Connect {

    static boolean foundId = false, foundPass = false;
    String[] coln = {"Username", "Firstname", "Lastname", "Weight", "Height"};
    DefaultTableModel model = new DefaultTableModel(coln, 0) {
    };

    Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:D:/Contact-ui/database/ContactDB.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return conn;
    }

    public boolean checkAddUser(String user) {
        String sql = "SELECT userId FROM User";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                String id = rs.getString("userId");
                return user.equals(id);
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }

    public int checkLogin(String recId, String recPass) {
        String sql = "SELECT userId, userPass FROM User";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                String id = rs.getString("userId");
                String password = rs.getString("userPass");
                if (recId.equals(id)) {
                    foundId = true;
                    if (password.equals(password)) {
                        foundPass = true;
                        break;
                    } else {
                        foundPass = false;
                        break;
                    }
                } else {
                    foundId = false;
                }
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return checkLoginFound(foundId, foundPass);
    }

    public int checkLoginFound(boolean foundId, boolean foundPass) {
        if (foundId == true && foundPass == true) {
            return 1;
        } else if (foundId == true && foundPass == false) {
            return 2;
        } else {
            return 3;
        }
    }

    public User selectForEdit(String user) {
        String sql = "SELECT userId, userPass, userFname, userLname, userWeight, userHeight FROM User";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            // loop through the result set
            while (rs.next()) {
                String Username = rs.getString("userId");
                String Password = rs.getString("userPass");
                String Firstname = rs.getString("userFname");
                String Lastname = rs.getString("userLname");
                Double Weight = rs.getDouble("userWeight");
                Double Height = rs.getDouble("userHeight");
                if (user.equals(Username)) {
                    User arr = new User(Username, Password, Firstname, Lastname, Weight, Height);
                    return arr;
                }

            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    public DefaultTableModel selectForShowTable() {
        String sql = "SELECT userId, userFname, userLname, userWeight, userHeight FROM User";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            // loop through the result set
            while (rs.next()) {
                String Username = rs.getString("userId");
                String Firstname = rs.getString("userFname");
                String Lastname = rs.getString("userLname");
                Double Weight = rs.getDouble("userWeight");
                Double Height = rs.getDouble("userHeight");
                model.addRow(new Object[]{Username, Firstname, Lastname, Weight, Height});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }

    public void update(String userId, String userPass, String userFname, String userLname, Double userWeight, Double userHeight) {
        String sql = "UPDATE User SET userPass = ? , " + "userFname = ? ," + "userLname = ? ,"
                + "userWeight = ? ," + "userHeight = ? "
                + "WHERE userId = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, userPass);
            pstmt.setString(2, userFname);
            pstmt.setString(3, userLname);
            pstmt.setDouble(4, userWeight);
            pstmt.setDouble(5, userHeight);
            pstmt.setString(6, userId);
            // update 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void insert(String userId, String userPass, String userFname, String userLname, Double userWeight, Double userHeight) {
        String sql = "INSERT INTO User(userId, userPass, userFname, userLname, userWeight, userHeight) VALUES(?,?,?,?,?,?)";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, userId);
            pstmt.setString(2, userPass);
            pstmt.setString(3, userFname);
            pstmt.setString(4, userLname);
            pstmt.setDouble(5, userWeight);
            pstmt.setDouble(6, userHeight);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void delete(String userId) {
        String sql = "DELETE FROM User WHERE userId = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, userId);
            // execute the delete statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

}
