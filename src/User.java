/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jainch
 */
public class User {

    String user;
    String pass;
    String fname;
    String lname; 
    double weight;
    double height;

    public User(String user, String pass, String fname, String lname, double weight, double height) {
        this.user = user;
        this.pass = pass;
        this.fname = fname;
        this.lname = lname;
        this.weight = weight;
        this.height = height;
    }

    public String toString() {
        String strUser = "Username : " + user;
        String strPass = "\nPassword : " + pass;
        String strFname = "\nFirstname : " + fname;
        String strLname = "\nLastname : " + lname;
        String strWeight = "\nWeight : " + Double.toString(weight);
        String strHeight = "\nHeight : " + Double.toString(height);
        return strUser + strPass + strFname + strLname + strWeight + strHeight;

    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public Double getWeight() {
        return weight;
    }

    public Double getHeight() {
        return height;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public void setLname(String lname) {
        this.lname = lname;

    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
