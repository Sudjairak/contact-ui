
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jainch
 */
public class AddFrame extends javax.swing.JFrame {

    /**
     * Creates new form addFrame
     */
    static String addUser, addPass, addFname, addLname, addWeight, addHeight;

    public AddFrame() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        LabelAdd = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        labelUsername = new javax.swing.JLabel();
        labelPassword = new javax.swing.JLabel();
        labelFirstname = new javax.swing.JLabel();
        labelLastname = new javax.swing.JLabel();
        labelWeight = new javax.swing.JLabel();
        labelHeight = new javax.swing.JLabel();
        textFieldUser = new javax.swing.JTextField();
        textFieldPass = new javax.swing.JTextField();
        textFieldFname = new javax.swing.JTextField();
        textFieldLname = new javax.swing.JTextField();
        textFieldWeight = new javax.swing.JTextField();
        textFieldHeight = new javax.swing.JTextField();
        buttonOK = new javax.swing.JButton();
        buttonCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        LabelAdd.setText("ADD");

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        labelUsername.setText("username:");

        labelPassword.setText("password:");

        labelFirstname.setText("firstname:");

        labelLastname.setText("lastname:");

        labelWeight.setText("weight:");

        labelHeight.setText("height:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelLastname)
                                    .addComponent(labelWeight)
                                    .addComponent(labelHeight))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(textFieldHeight, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
                                    .addComponent(textFieldWeight, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(textFieldLname)
                                        .addGap(1, 1, 1))))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(labelFirstname)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(textFieldFname, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(labelPassword)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(textFieldPass))))
                        .addContainerGap(106, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(labelUsername)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(textFieldUser, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 107, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelUsername)
                    .addComponent(textFieldUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelPassword)
                    .addComponent(textFieldPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelFirstname)
                    .addComponent(textFieldFname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelLastname)
                    .addComponent(textFieldLname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelWeight)
                    .addComponent(textFieldWeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelHeight)
                    .addComponent(textFieldHeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(43, Short.MAX_VALUE))
        );

        buttonOK.setText("OK");
        buttonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonOKActionPerformed(evt);
            }
        });

        buttonCancel.setText("Cancel");
        buttonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(buttonOK, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(buttonCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(182, 182, 182)
                            .addComponent(LabelAdd))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LabelAdd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonCancel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonOK, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(21, 21, 21))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonOKActionPerformed
        chooseAddUser();
        chooseAddHeight();
        chooseAddWeight();
        checkBooleanAdd();

    }//GEN-LAST:event_buttonOKActionPerformed

    private void buttonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancelActionPerformed
        this.dispose();
        getMenu();       // TODO add your handling code here:
    }//GEN-LAST:event_buttonCancelActionPerformed

    public void checkBooleanAdd() {
        if (chooseAddUser() == false) {
            showErrorAddUser();
            textFieldUser.setText("");
            textFieldUser.requestFocus();
        } else if (textFieldPass.getText().isEmpty()) {
            showErrorAddPass();
            textFieldPass.setText("");
            textFieldPass.requestFocus();
        } else if (chooseAddFname() == false) {
            showErrorAddName();
            textFieldFname.setText("");
            textFieldFname.requestFocus();
        } else if (chooseAddLname() == false) {
            showErrorAddName();
            textFieldLname.setText("");
            textFieldLname.requestFocus();
        } else if (chooseAddWeight() == false) {
            showErrorAddWeight();
            textFieldWeight.setText("");
            textFieldWeight.requestFocus();
        } else if (chooseAddHeight() == false) {
            showErrorAddHeight();
            textFieldHeight.setText("");
            textFieldHeight.requestFocus();
        } else {
            add();
            showSuccess();
        }
    }

    public void add() {
        addUser = textFieldUser.getText();
        addPass = textFieldPass.getText();
        addFname = textFieldFname.getText();
        addLname = textFieldLname.getText();
        addWeight = textFieldWeight.getText();
        addHeight = textFieldHeight.getText();
        Connect app = new Connect();
        app.insert(addUser, addPass, addFname, addLname, Double.valueOf(addWeight), Double.valueOf(addHeight));
    }

    public void showSuccess() {
        JOptionPane.showMessageDialog(null, "Success.", "information",
                JOptionPane.INFORMATION_MESSAGE);
        this.dispose();
        getMenu();

    }

    public void showErrorAddUser() {
        JOptionPane.showMessageDialog(null, "User has been existed.", "alert", JOptionPane.ERROR_MESSAGE);
    }

    public void showErrorAddPass() {
        JOptionPane.showMessageDialog(null, "Password must not empty.", "alert", JOptionPane.ERROR_MESSAGE);
    }

    public void showErrorAddName() {
        JOptionPane.showMessageDialog(null, "Name must be String.", "alert", JOptionPane.ERROR_MESSAGE);
    }

    public void showErrorAddHeight() {
        JOptionPane.showMessageDialog(null, "Height must be number.", "alert", JOptionPane.ERROR_MESSAGE);
    }

    public void showErrorAddWeight() {
        JOptionPane.showMessageDialog(null, "Weight must be number.", "alert", JOptionPane.ERROR_MESSAGE);
    }

    public boolean chooseAddUser() {
        Connect app = new Connect();
        return !app.checkAddUser(textFieldUser.getText());
    }

    public boolean chooseAddFname() {
        String regex = "^[a-zA-Z ,.'-]+$";
        addFname = textFieldFname.getText();
        return addFname.matches(regex);
    }

    public boolean chooseAddLname() {
        String regex = "^[a-zA-Z ,.'-]+$";
        addLname = textFieldLname.getText();
        return addLname.matches(regex);
    }

    public boolean chooseAddWeight() {
        String regex = "\\d+\\.\\d*|\\.?\\d+";
        addWeight = textFieldWeight.getText();
        return addWeight.matches(regex);
    }

    public boolean chooseAddHeight() {
        String regex = "\\d+\\.\\d*|\\.?\\d+";
        addHeight = textFieldHeight.getText();
        return addHeight.matches(regex);
    }

    public void getMenu() {
        MenuFrame nm = new MenuFrame();
        nm.pack();
        nm.setLocationRelativeTo(null);
        nm.setVisible(true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AddFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AddFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AddFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AddFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            AddFrame am = new AddFrame();
            am.pack();
            am.setLocationRelativeTo(null);
            am.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelAdd;
    private javax.swing.JButton buttonCancel;
    private javax.swing.JButton buttonOK;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel labelFirstname;
    private javax.swing.JLabel labelHeight;
    private javax.swing.JLabel labelLastname;
    private javax.swing.JLabel labelPassword;
    private javax.swing.JLabel labelUsername;
    private javax.swing.JLabel labelWeight;
    private javax.swing.JTextField textFieldFname;
    private javax.swing.JTextField textFieldHeight;
    private javax.swing.JTextField textFieldLname;
    private javax.swing.JTextField textFieldPass;
    private javax.swing.JTextField textFieldUser;
    private javax.swing.JTextField textFieldWeight;
    // End of variables declaration//GEN-END:variables
}
